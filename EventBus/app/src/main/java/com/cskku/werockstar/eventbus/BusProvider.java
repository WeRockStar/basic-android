package com.cskku.werockstar.eventbus;

import com.squareup.otto.Bus;

/**
 * Created by Kotchaphan on 22/10/2558.
 */
public class BusProvider {
    public static Bus bus;

    public static Bus getBus() {
        if (bus == null)
            bus = new Bus();

        return bus;
    }
}
