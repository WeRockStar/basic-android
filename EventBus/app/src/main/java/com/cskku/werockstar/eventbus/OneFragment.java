package com.cskku.werockstar.eventbus;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.squareup.otto.Bus;


/**
 * A simple {@link Fragment} subclass.
 */
public class OneFragment extends Fragment implements View.OnClickListener {


    public OneFragment() {
        // Required empty public constructor
    }


    Button btnOne;
    Bus bus;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_one, container, false);

        if (bus == null)
            bus = BusProvider.getBus();

        bus.register(this);

        btnOne = (Button) view.findViewById(R.id.btnOne);
        btnOne.setOnClickListener(this);
        return view;
    }


    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.btnOne:
                Message message = new Message();
                message.setMessage("From one fragment");
                bus.post(message);
                break;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        bus.unregister(this);
    }
}
