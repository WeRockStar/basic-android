package com.cskku.werockstar.eventbus;

import android.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

public class MainActivity extends AppCompatActivity {

    Bus bus;
    TextView txt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Fragment fragment = getFragmentManager().findFragmentById(R.id.fragmentOne);
        OneFragment oneFragment = (OneFragment)fragment;

        if (bus == null)
            bus = BusProvider.getBus();

        txt = (TextView) findViewById(R.id.txtMain);

        bus.register(this);
    }

    @Subscribe
    public void getMessageFromFragments(Message message) {
        txt.setText(message.getMessage());
    }

    @Override
    protected void onPause() {
        super.onPause();
        bus.unregister(this);
    }
}
