package com.cskku.werockstar.eventbus;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.squareup.otto.Bus;


/**
 * A simple {@link Fragment} subclass.
 */
public class TwoFragment extends Fragment implements View.OnClickListener {


    public TwoFragment() {
        // Required empty public constructor
    }

    Bus bus;
    Button btnTwo;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_two, container, false);
        if (bus == null)
            bus = BusProvider.getBus();
        bus.register(this);

        btnTwo = (Button) view.findViewById(R.id.btnTwo);
        btnTwo.setOnClickListener(this);

        return view;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        bus.unregister(this);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.btnTwo:
                Message message = new Message();
                message.setMessage("From two fragment");
                bus.post(message);
                break;
        }
    }
}
