package com.cskku.werockstar.interativewithactivity;


import android.content.Context;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class OneFragment extends Fragment {

    onClickFragment listener;

    public OneFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_one, container, false);

        try {
            listener = (onClickFragment) getActivity();
        } catch (ClassCastException e) {

        }
        Button button = (Button) view.findViewById(R.id.btnSend);
        final EditText editText = (EditText) view.findViewById(R.id.edtText);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Message message = new Message();
                message.setMessage(editText.getText().toString());
                listener.messsage(message);
            }
        });
        return view;
    }


    public interface onClickFragment {
        public void messsage(Message message);
    }

}
