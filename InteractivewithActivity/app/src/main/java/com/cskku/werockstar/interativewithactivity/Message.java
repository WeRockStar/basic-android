package com.cskku.werockstar.interativewithactivity;

/**
 * Created by Kotchaphan on 22/10/2558.
 */
public class Message {
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
