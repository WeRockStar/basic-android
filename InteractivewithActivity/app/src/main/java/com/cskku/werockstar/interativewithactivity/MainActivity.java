package com.cskku.werockstar.interativewithactivity;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements OneFragment.onClickFragment {

    TextView txt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txt = (TextView) findViewById(R.id.txt);

        Fragment fragment = getFragmentManager().findFragmentById(R.id.one);
        OneFragment oneFragment = (OneFragment) fragment;

    }

    @Override
    public void messsage(Message message) {
        txt.setText(message.getMessage());
    }

}
