package com.cskku.werockstar.picassoglide;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button mPicasso;
    Button mGlide;
    Button mPicassoGlide;

    ImageView mImagePicasso;
    ImageView mImageGlide;

    //String url = "https://c2.staticflickr.com/4/3841/14781942381_9df32e51e4_o.jpg";
    String url = "https://snap-photos.s3.amazonaws.com/img-thumbs/960w/G0VD2SOSL6.jpg";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initialView();

        mPicasso.setOnClickListener(this);
        mGlide.setOnClickListener(this);
        mPicassoGlide.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.btnPicasso:
                picassoLoad();
                break;
            case R.id.btnGlide:
                picassoLoad();
                break;
            case R.id.btnPicassoGlide:
                picassoLoad();
                glideLoad();
                break;
        }
    }

    public void initialView() {
        mPicasso = (Button) findViewById(R.id.btnPicasso);
        mGlide = (Button) findViewById(R.id.btnGlide);
        mPicassoGlide = (Button) findViewById(R.id.btnPicassoGlide);

        mImageGlide = (ImageView) findViewById(R.id.imgGlide);
        mImagePicasso = (ImageView) findViewById(R.id.imgPicasso);
    }

    public void picassoLoad() {
        Picasso.with(MainActivity.this).load(url).resize(150, 150).centerCrop().into(mImagePicasso);
    }

    public void glideLoad() {
        Glide.with(MainActivity.this).load(url).centerCrop().into(mImageGlide);
    }
}
