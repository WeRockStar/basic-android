package com.cskku.werockstar.okhttp;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

public class MainActivity extends AppCompatActivity {

    Button btnAdd;
    EditText edtMessage;
    ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnAdd = (Button) findViewById(R.id.btnAdd);
        edtMessage = (EditText) findViewById(R.id.edtMessage);

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new InsertMessage().execute();
            }
        });
    }

    class InsertMessage extends AsyncTask<Void, Void, Void> {

        OkHttpClient client = new OkHttpClient();
        RequestBody body;
        Request request;
        Response response;
        String message;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            message = edtMessage.getText().toString();
            dialog = new ProgressDialog(MainActivity.this);
            dialog.setIndeterminate(false);
            dialog.setMessage("Loading...");
            dialog.show();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            dialog.cancel();
            edtMessage.setText("");
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                body = new FormEncodingBuilder().add("message", message).build();
                request = new Request.Builder().url("http://basicandroid.azurewebsites.net/libs.php").post(body).build();
                response = client.newCall(request).execute();


            } catch (Exception e) {
                Log.i("ERROR", e.getMessage());
            }
            return null;
        }
    }
}
